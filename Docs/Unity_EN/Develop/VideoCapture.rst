.. role:: raw-html-m2r(raw)
   :format: html


Video Capture
--------------

:raw-html-m2r:`<br />`

Getting Started
^^^^^^^^^^^^^^^

If you dont know how to use NRSDK. You can see this :ref:`Quickstart for Android<quickstart_android>` to have a quick start with NRSDK.

:raw-html-m2r:`<br />`

Import the NRSDK for First Person View
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363> Import</font>` **NRSDKForUnity_1.4.8.unitypackage**\ :raw-html-m2r:`<font color=#636363> in the package.</font>`

:raw-html-m2r:`<br />`

Open the Sample Scene
^^^^^^^^^^^^^^^^^^^^^

In the Unity Project window, you can find the CameraCaptureDemo sample in: ``Assets > NRSDK >Demos > RGBCamera-Record.``

:raw-html-m2r:`<br />`

Build and Run the Sample App
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

..

   See ``CameraCaptureController.cs`` , located in ``Assets > NRSDK > Demos > Record > Scripts > VideoCaptureLocalExample`` for an example on how to use the video capture. 

    
.. code-block:: c#

       // A video Capture Example
       public class VideoCapture2LocalExample : MonoBehaviour
       {
           /// <summary> The previewer. </summary>
        public NRPreviewer Previewer;
        public VideoRecordConfigPanel m_ConfigPanel;

        /// <summary> Save the video to Application.persistentDataPath. </summary>
        /// <value> The full pathname of the video save file. </value>
        public string VideoSavePath
        {
            get
            {
                string timeStamp = Time.time.ToString().Replace(".", "").Replace(":", "");
                string filename = string.Format("Nreal_Record_{0}.mp4", timeStamp);
                return Path.Combine(Application.persistentDataPath, filename);
            }
        }

        /// <summary> The video capture. </summary>
        NRVideoCapture m_VideoCapture = null;

        /// <summary> Starts this object. </summary>
        void Start()
        {
            CreateVideoCaptureTest();
        }

        /// <summary> Tests create video capture. </summary>
        void CreateVideoCaptureTest()
        {
            NRVideoCapture.CreateAsync(false, delegate (NRVideoCapture videoCapture)
            {
                NRDebugger.Info("Created VideoCapture Instance!");
                if (videoCapture != null)
                {
                    m_VideoCapture = videoCapture;
                }
                else
                {
                    NRDebugger.Error("Failed to create VideoCapture Instance!");
                }
            });
        }

        /// <summary> Starts video capture. </summary>
        public void StartVideoCapture()
        {
            if (m_VideoCapture != null)
            {
                CameraParameters cameraParameters = new CameraParameters();
                if (m_ConfigPanel == null)
                {
                    Resolution cameraResolution = NRVideoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
                    cameraParameters.hologramOpacity = 0.0f;
                    cameraParameters.frameRate = cameraResolution.refreshRate;
                    cameraParameters.cameraResolutionWidth = cameraResolution.width;
                    cameraParameters.cameraResolutionHeight = cameraResolution.height;
                    cameraParameters.pixelFormat = CapturePixelFormat.BGRA32;
                    // Set the blend mode.
                    cameraParameters.blendMode = BlendMode.Blend;
                    // Set audio state, audio record needs the permission of "android.permission.RECORD_AUDIO",
                    // Add it to your "AndroidManifest.xml" file in "Assets/Plugin".
                    cameraParameters.audioState = NRVideoCapture.AudioState.MicAudio;
                }
                else
                {
                    cameraParameters = m_ConfigPanel.GetRecordConfigration();
                }

                m_VideoCapture.StartVideoModeAsync(cameraParameters, OnStartedVideoCaptureMode);
            }
        }

        /// <summary> Stops video capture. </summary>
        public void StopVideoCapture()
        {
            if (m_VideoCapture == null)
            {
                return;
            }

            NRDebugger.Info("Stop Video Capture!");
            m_VideoCapture.StopRecordingAsync(OnStoppedRecordingVideo);
            Previewer.SetData(m_VideoCapture.PreviewTexture, false);
        }

        /// <summary> Executes the 'started video capture mode' action. </summary>
        /// <param name="result"> The result.</param>
        void OnStartedVideoCaptureMode(NRVideoCapture.VideoCaptureResult result)
        {
            if (!result.success)
            {
                NRDebugger.Info("Started Video Capture Mode faild!");
                return;
            }

            NRDebugger.Info("Started Video Capture Mode!");
            m_VideoCapture.StartRecordingAsync(VideoSavePath, OnStartedRecordingVideo);
            // Set preview texture.
            Previewer.SetData(m_VideoCapture.PreviewTexture, true);
        }

        /// <summary> Executes the 'started recording video' action. </summary>
        /// <param name="result"> The result.</param>
        void OnStartedRecordingVideo(NRVideoCapture.VideoCaptureResult result)
        {
            if (!result.success)
            {
                NRDebugger.Info("Started Recording Video Faild!");
                return;
            }

            NRDebugger.Info("Started Recording Video!");
            if (m_ConfigPanel != null && m_ConfigPanel.UseGreenBackground)
            {
                // Set green background color.
                m_VideoCapture.GetContext().GetBehaviour().SetBackGroundColor(Color.green);
            }
        }

        /// <summary> Executes the 'stopped recording video' action. </summary>
        /// <param name="result"> The result.</param>
        void OnStoppedRecordingVideo(NRVideoCapture.VideoCaptureResult result)
        {
            if (!result.success)
            {
                NRDebugger.Info("Stopped Recording Video Faild!");
                return;
            }

            NRDebugger.Info("Stopped Recording Video!");
            m_VideoCapture.StopVideoModeAsync(OnStoppedVideoCaptureMode);
        }

        /// <summary> Executes the 'stopped video capture mode' action. </summary>
        /// <param name="result"> The result.</param>
        void OnStoppedVideoCaptureMode(NRVideoCapture.VideoCaptureResult result)
        {
            NRDebugger.Info("Stopped Video Capture Mode!");
        }
       }

* Audio record needs the permission of "android.permission.RECORD_AUDIO", Add it to your "AndroidManifest.xml" file in "Assets/Plugin".
* The **Previewer** is used to preview live images in real time, for debugging purposes. Click the **APP** key of the controller to show or hide it.
* Click the **Start** button  to start video capture.
* Click the **Stop** button  to stop video capture. It will save the video file to **"Application.persistentDataPath"** path.
