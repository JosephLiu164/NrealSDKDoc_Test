.. role:: raw-html-m2r(raw)
   :format: html

.. _controller_guide:

Controller
----------

:raw-html-m2r:`<font color=#636363>This guide will show you how to use the</font>` **Nreal Light controller** :raw-html-m2r:`<font color=#636363>and</font>`  **Nreal phone controller**\ :raw-html-m2r:`<font color=#636363>. The tutorial will demonstrate how to create apps using</font>` **Nreal Light controller** :raw-html-m2r:`<font color=#636363>as an interaction model.</font>`

:raw-html-m2r:`<br />`

Introduction
^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>Controllers provide a simple way for users to navigate in mixed reality. In the latest version, NRSDK supports two types of controlling methods:</font>`  **Nreal Light controller** :raw-html-m2r:`<font color=#636363>and</font>`  **Nreal phone controller**\ :raw-html-m2r:`<font color=#636363>. The Nreal Light controller can be paired with a Nreal Light computing unit or a mobile phone through Bluetooth. The Nreal phone controller is only available when using the phone as Nreal Light's computing unit.</font>` 

:raw-html-m2r:`<font color=#636363>Currently, only the</font>`  **Nreal Light controller**\ :raw-html-m2r:`<font color=#636363> (available in the Nreal Developer Kit) is available to developers. The</font>`  **Nreal phone controller** :raw-html-m2r:`<font color=#636363>will serve as a second input choice for users in the future.</font>` 

:raw-html-m2r:`<br />`

How to Use the Nreal Light Controller
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


|image1|


:raw-html-m2r:`<br />`

The Nreal Light Controller
~~~~~~~~~~~~~~~~~~~~~~~~~~


* **On the front**

.. list-table::
   :header-rows: 1

   * - Controller Features
     - Description
   * - :raw-html-m2r:`<font color=#636363>3DoF Tracking</font>`
     - :raw-html-m2r:`<font color=#636363>`\ 3 degrees of freedom
   * - :raw-html-m2r:`<font color=#636363>Touchpad</font>`
     - **Press** :raw-html-m2r:`<font color=#636363>= Trigger Button. Can detect touching and clicking. Customizable for your app, e.g. scrolling.</font>`
   * - :raw-html-m2r:`<font color=#636363>App Button</font>`
     - **Press and hold** :raw-html-m2r:`<font color=#636363>to recenter your controller.</font>` **Press** :raw-html-m2r:`<font color=#636363>can be customized for your app, e.g. opening an in-app menu or performing a special action. (</font>` **Pay attention:** :raw-html-m2r:`<font color=#636363> For Nreal Light Controller, pressing the App Button equals to pressing the Touchpad Button)</font>`
   * - :raw-html-m2r:`<font color=#636363>Home Button</font>`
     - **Press and hold** to open Exit App Window.</font> **Press** :raw-html-m2r:`<font color=#636363>can be customized for in-app action, e.g. return to the previous step.</font>`
   * - :raw-html-m2r:`<font color=#636363>LED Light</font>`
     - :raw-html-m2r:`<font color=#636363>Display controller status</font>`


:raw-html-m2r:`<br />`


* **On the back**

.. list-table::
   :header-rows: 1

   * - Controller Features
     - Description
   * - :raw-html-m2r:`<font color=#636363>Charging pins</font>`
     - :raw-html-m2r:`<font color=#636363>The three bottom-facing charging pins connect the controller with the computing unit. It is also used to send data from the computing unit to the controller.</font>`
   * - :raw-html-m2r:`<font color=#636363>Power Switch</font>`
     - :raw-html-m2r:`<font color=#636363>The Nreal Light controller  on/off switch is below the charging spots. Green means the power is on, and red means the power is off.</font>`


:raw-html-m2r:`<br />`


* **Nreal Light Controller LED Guide**

:raw-html-m2r:`<font color=#636363>See</font>` **LED demo** :raw-html-m2r:`<font color=#636363>below:</font>`


|image2|


:raw-html-m2r:`<br />`


* **Connecting the Nreal Light Controller**


#. 
   :raw-html-m2r:`<font color=#636363>Make sure the Bluetooth on your Neal Light computing unit/or Android mobile phone is on. (The Bluetooth on Nreal Light computing unit is on as default.)</font>`

#. 
   :raw-html-m2r:`<font color=#636363>Turn the Nreal Light controller power switch on. Long press the touchpad until the green light starts blinking.</font>`

#. 
   :raw-html-m2r:`<font color=#636363>Put the Nreal Light controller on the computing unit, and they will be paired automatically.</font>`

#. 
   :raw-html-m2r:`<font color=#636363>If pairing is successful, the LED will stop blinking and become solid green. If you cannot complete this step, remove all other controllers in the Bluetooth record list and try again.</font>`

:raw-html-m2r:`<br />`


* **Charging the Nreal Light Controller**


|image3|


:raw-html-m2r:`<br />`

:raw-html-m2r:`<font color=#636363>Magnetically attach to the Nreal Light computing unit. Battery power will be shared between the two whether the controller is on or off.</font>`

`The lithium battery in the controller is not removable. Please do not attempt to open the controller. Damage caused this way cannot be fixed.`

:raw-html-m2r:`<br />`

How to Use Your Phone as a Controller
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


|image4|


:raw-html-m2r:`<br />`

.. list-table::
   :header-rows: 1

* - Controller Features
 - Description
* - :raw-html-m2r:`<font color=#636363>3DoF Tracking</font>`
 - :raw-html-m2r:`<font color=#636363>3 degrees of freedom</font>`
* - :raw-html-m2r:`<font color=#636363>Touchpad</font>`
 - **Press** :raw-html-m2r:`<font color=#636363>= Trigger Button. Can detect touching and clicking. Customizable for your app, e.g. scrolling.</font>`
* - :raw-html-m2r:`<font color=#636363>App Button</font>`
 - **Press and hold** :raw-html-m2r:`<font color=#636363>to recenter your controller.</font>` **Press** :raw-html-m2r:`<font color=#636363>can be customized for your app, e.g. opening an in-app menu or performing a special action.</font>`
* - :raw-html-m2r:`<font color=#636363>Home Button</font>`
 - **Press and hold** :raw-html-m2r:`<font color=#636363>to open Exit App Window.</font>`  **Press** :raw-html-m2r:`<font color=#636363>can be customized for in-app action, e.g. return to the previous step.</font>`


:raw-html-m2r:`<br />`

Tutorial： Nreal Light Controller
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Input in Unity
~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>These following points will help you better understand the Input from NRSDK in Unity.</font>`

**ControllerProvider:** :raw-html-m2r:`<font color=#636363>This provides various controller state information to NRInput, including rotation, button press, button up, battery, and gyro. There is a default provider for Nreal controllers. You can also write a custom controller provider.</font>`

**EventSystem:**\ :raw-html-m2r:`<font color=#636363> Integration with Unity's standard EventSystem supports user interaction with GUI and other physical objects. NRIput will automatically generate an EventSystem when the application runs.</font>`

**Visualizations:** :raw-html-m2r:`<font color=#636363>Default visualizations, such as controller visual, ray visual, and reticle visual, are already included. There is also an interface for you to reskin visualizations.</font>`

:raw-html-m2r:`<br />`

Enabling NRInput
~~~~~~~~~~~~~~~~


To enable NRInput, simply drag the **NRInput** prefab into your scene hierarchy.
If you already have a Main Camera set up, drag it onto **Override Camera Center** in the NRInput prefab. Otherwise, the prefab will find the Main Camera.
Users can now interact in the app using Nreal Light controllers.


|image5|


:raw-html-m2r:`<br />`

Raycast Modes in NRInput
~~~~~~~~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>There are two raycasting modes included in the NRInput: the <u>Gaze Mode</u> and the <u>Ray Mode</u>.</font>`

:raw-html-m2r:`<font color=#636363>The <u>Ray Mode</u> is the default raycasting mode most apps will be based on. In this mode, the ray will start from the center of the controller.</font>`

:raw-html-m2r:`<font color=#636363>When in <u>Gaze Mode</u>, the raycast will start from the center of the user's forehead and emanate in a forward direction. Thus, the ray will be invisible.</font>`

:raw-html-m2r:`<br />`

Current Controller Features
~~~~~~~~~~~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>Since NRSDK supports multiple types of controllers, a method was scripted to help you understand the features your current controller supports.</font>`


* **Sample Use Case:**

.. code-block:: c#

       bool supportsPosition = NRInput.GetControllerAvailableFeature(ControllerAvailableFeature.CONTROLLER_AVAILABLE_FEATURE_POSITION);
       bool supportsGyro = NRInput.GetControllerAvailableFeature(ControllerAvailableFeature.CONTROLLER_AVAILABLE_FEATURE_GYRO);

Some Common Usage of NRInput
~~~~~~~~~~~~~~~~~~~~~~~~~~~~


* **Sample Use Case:**

.. code-block:: c#

   void Update()
       {
           //returns a int value of how many available controllers currently
           NRInput.GetAvailableControllersCount();

           //returns true if a Trigger button is currently pressed
           NRInput.GetButton(ControllerButton.TRIGGER);

           //returns true if a Trigger button was pressed down this frame
           NRInput.GetButtonDown(ControllerButton.TRIGGER);

           //returns true if a Trigger button was released this frame
           NRInput.GetButtonUp(ControllerButton.TRIGGER);

           //returns Vector3.zero if controller is 3DoF, otherwise returns the position of the domain controller
           NRInput.GetPosition();

           //returns the rotation of the domain controller
           NRInput.GetRotation();

           //returns the ControllerType of the domain controller
           ControllerType controllerType = NRInput.GetControllerType();

           //returns true if the touchpad is being touched
           NRInput.IsTouching();

           //returns a Vector2 value of the touchpad, x(-1f ~ 1f), y(-1f ~ 1f)
           NRInput.GetTouch();

           //returns a Vector3 value of gyro if supports
           NRInput.GetGyro();

           //to get what hand mode is using now, left-hand or right-hand
           ControllerHandEnum domainHand = NRInput.DomainHand;

           //the same as NRInput.GetRotation()
           NRInput.GetRotation(NRInput.DomainHand);

           //returns the rotaion of the left hand controller
           NRInput.GetRotation(ControllerHandEnum.Right);
       }

:raw-html-m2r:`<br />`

Get Frequently Used Anchors
~~~~~~~~~~~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>The NRInput provides an easy way to get frequently used anchors quickly.</font>`


* **Sample Use Case:**

.. code-block:: c#

       Transform gazeAnchor = NRInput.AnchorsHelper.GetAnchor(ControllerAnchorEnum.GazePoseTrackerAnchor);
       Transform leftRayAnchor = NRInput.AnchorsHelper.GetAnchor(ControllerAnchorEnum.LeftRayAnchor);

:raw-html-m2r:`<br />`

Raycasters in NRInput
~~~~~~~~~~~~~~~~~~~~~

:raw-html-m2r:`<font color=#636363>Three raycasters are included in the NRInput prefab. One of which for Gaze Mode, and the other two for Ray Mode. The raycaster class inherits from Unity's BaseRaycaster class. A chosen raycaster's farthest raycasting distance can be modified directly from the Inspector window. You can also define which objects are interactable by changing the parameter of its Mask.</font>`


|image6|


Building a Project with User Input
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:raw-html-m2r:`<font color=#636363>To help you get started with integrating control into your own app, we have prepared a sample case study.</font>`

:raw-html-m2r:`<font color=#636363>By reading the study you will be able to:</font>`


#. :raw-html-m2r:`<font color=#636363>Select a range of appropriate inputs to use with the controller.</font>`
#. :raw-html-m2r:`<font color=#636363>Write code in C# to receive user inputs such as the Home, Bumper, Trigger and Touchpad.</font>`
#. :raw-html-m2r:`<font color=#636363>Activate, rotate, and move a GameObject by using the input.</font>`
#. :raw-html-m2r:`<font color=#636363>Interact with physical objects and UGUI in the unity environment.</font>`

**Objective:**
~~~~~~~~~~~~~~~~~~


#. :raw-html-m2r:`<font color=#636363>When pointing the controller at the cube, its color will change to green.</font>`
#. :raw-html-m2r:`<font color=#636363>The color of the cube will randomly change when you press the trigger button.</font>`
#. :raw-html-m2r:`<font color=#636363>The cube will rotate with the controller.</font>`
#. :raw-html-m2r:`<font color=#636363>Activate or deactivate the cube for rotation by pressing the UGUI button.</font>`

**Steps：**
~~~~~~~~~~~~~~~


1. Drag NRCameraRig and the NRInput prefab to the scene hierarchy.


|image7|


2. Create a cube. In the Inspector window, set its position to (0, 0.1, 3), and scale to (0.3, 0.3, 0.3).

|image8|

|image9|


3. Create a script named **"CubeInteractiveTest.cs"** as follows and add it to the cube.

   .. code-block:: c#

      public class CubeInteractiveTest : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
      {
       private MeshRenderer m_MeshRender;

       void Awake () {
           m_MeshRender = transform.GetComponent<MeshRenderer>();
       }

       void Update()
       {
           //get controller rotation, and set the value to the cube transform
           transform.rotation = NRInput.GetRotation();
       }

       //when pointer click, set the cube color to random color
       public void OnPointerClick(PointerEventData eventData)
       {
           m_MeshRender.material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
       }

       //when pointer hover, set the cube color to green
       public void OnPointerEnter(PointerEventData eventData)
       {
           m_MeshRender.material.color = Color.green;
       }

       //when pointer exit hover, set the cube color to white
       public void OnPointerExit(PointerEventData eventData)
       {
           m_MeshRender.material.color = Color.white;
       }
      }


   |image10|



4. Add a DirectLight to brighten the cube.

|image11|


5. Add a Canvas and delete the EventSystem.


|image12|

|image13|



6. Set the Canvas RenderMode to WorldSpace, and then set its position and scale to an appropriate value such as that listed below.

|image14|

7. Remove the **"Graphic Raycaster"** component from it and add <u>Canvas Raycast Target</u>.

|image15|

|image16|

8. Add two buttons to the Canvas as its children. Name one button <u>activeBtn</u> and the other <u>deactive</u>. Remember to also change the button text.

|image17|

9. Set the buttons to an appropriate position and add the <u>SetActive</u> function as their OnClick events.

|image18|


10. Finally, compile the app. An interactive mixed reality app utilizing user input with a Nreal Light controller is complete.

|image19|



.. |image1| image:: ../../../images/controller01.png
.. |image2| image:: ../../../images/controller02.gif
.. |image3| image:: ../../../images/controller03.gif
.. |image4| image:: ../../../images/controller04.png
.. |image5| image:: ../../../images/controller05.jpg
.. |image6| image:: ../../../images/controller06.jpg
.. |image7| image:: ../../../images/controller07.jpg
.. |image8| image:: ../../../images/controller08.jpg
.. |image9| image:: ../../../images/controller09.jpg
.. |image10| image:: ../../../images/controller10.jpg
.. |image11| image:: ../../../images/controller11.jpg
.. |image12| image:: ../../../images/controller12.jpg
.. |image13| image:: ../../../images/controller13.jpg
.. |image14| image:: ../../../images/controller14.jpg
.. |image15| image:: ../../../images/controller15.jpg
.. |image16| image:: ../../../images/controller16.jpg
.. |image17| image:: ../../../images/controller17.jpg
.. |image18| image:: ../../../images/controller18.jpg
.. |image19| image:: ../../../images/controller19.jpg