.. role:: raw-html-m2r(raw)
   :format: html


RGB Camera
----------

:raw-html-m2r:`<font color=#636363>Follow this guide to learn how to build a sample app to retrieve RGB camera data from Nreal Light glasses through NRSDK.</font>`

:raw-html-m2r:`<br />`

Open the Sample Scene
^^^^^^^^^^^^^^^^^^^^^


* In the Unity Project window, you can find the **CameraCaptureDemo** sample in: ``Assets > NRSDK >Demos > RGBCamera``.

:raw-html-m2r:`<br />`

Build and Run the Sample App
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Enable developer options and USB debugging on your Nreal Light computing unit. **Android Debug Bridge** `(adb) <https://developer.android.com/studio/command-line/adb>`_ is enabled as default.

* Connect your Nreal Light computing unit to your Windows PC.

* 
  :raw-html-m2r:`<font color=#636363>In the Unity</font>` **Build Settings** :raw-html-m2r:`<font color=#636363>window, click</font>` **Build and Run**.

..

   See ``CameraCaptureController.cs`` , located in ``Assets > NRSDK > Demos > CameraCapture > Scripts`` for an example on how to get the texture of RGB Camera. 



* Initialize **NRCameraCapture** in MonoBehaviour and configure it as follows:

  .. code-block:: c#

      public RawImage CaptureImage;

       private void Start()
       {
           RGBCamTexture = new NRRGBCamTexture();
           CaptureImage.texture = RGBCamTexture.GetTexture();
           RGBCamTexture.Play();
       }
