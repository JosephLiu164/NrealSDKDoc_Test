.. _Learn_Nreal_Glasses:

Learn Nreal Glasses
===================

Nreal's products include the Nreal Light Developer Kit, Nreal Light Consumer Kit, and Nreal Air. See below for a brief overview of each headset, the features, and attributes relevant to developers.

* **Nreal Light**: The consumer version of Nreal Light, which is currently selling through its partnered carriers’ retail stores. Nreal Light operates by tethering to an Android smartphone.

* **Nreal Air**:  Compared to Nreal Light, Nreal Air is a pared down, yet highly fashionable pair of AR glasses, that pushes the envelope with a form factor that’s wearable and inherently more comfortable.

* **Nreal Light Developer Kit**: The developer version of Nreal Light is designed for developers to build MR apps on the platform. The Nreal Light Developer Kit operates by tethering to a proprietary computing pack developed by Nreal.


Compare Nreal Glasses
---------------------
See below for a detailed set of technical specs for Nreal Light and Nreal Air.

.. list-table::
   :header-rows: 1

   * -
     - Nreal Light
     - Nreal Air
   * - Size(folded)
     - 156mm * 52mm * 44mm
     - 151mm * 41mm * 51mm
   * - Weight (Excluding cable)
     - 106g
     - ~77g
   * - Resolution Per Eye
     - 1920 * 1080
     - 1920 * 1080
   * - FOV
     - 52 Degrees
     - 46 Degrees
   * - Frame Rate - MR Space
     - 60Hz
     - 60Hz; 72Hz in future release
   * - Supporting System
     - Android
     - Android
   * - RGB Camera
     - yes
     - n.a.
   * - Grayscale Camera
     - yes
     - n.a.
   * - IMU
     - yes
     - yes
   * - Connection
     - USB-C Cable
     - USB-C Cable
   * - Audio
     - Dual Speakers and Microphones
     - Dual Speakers and Microphones


NRSDK Supported Features
------------------------
See below for a comparison of feature set supported by NRSDK:

.. list-table::
   :header-rows: 1

   * -
     - Nreal Light
     - Nreal Air
   * - Motion Tracking
     - 6DoF
     - 3DoF
   * - Plane Tracking
     - support
     - n.a.
   * - Image Tracking
     - support
     - n.a.
   * - Hand Tracking
     - support
     - n.a.
   * - Screen Capture
     - Superimposed application content
     - Application Content
   * - Access to RGB Camera
     - support
     - n.a.
   * - Controller
     - 3DoF
     - 3DoF
   * - Customize Phone Controller
     - support
     - support
   * - Emulator Testing
     - support
     - support
   * - Customize Notification Popup
     - support
     - support

It is worth noting that **all the features supported by Nreal Air are supported by Nreal Light**. By default, NRSDK will automatically adapt for Nreal Air within device's capability and try to run the application properly, even if you'd applied an NRSDK feature not supported by Nreal Air.

Nevertheless, be aware that the actual behavior of the running application may vary through this way and differ from your initial intent.

If you don't want NRSDK to automatically adapt for Nreal Air, please specify the supported devices in NRSDK's global configuration file ``NRKernalSessionConfig``. See :ref:`Quickstart for Android<quickstart_android>` for more details.

.. |image0| image:: ../../../images/Discover/introduction-nrsdk01.jpg
.. |image1| image:: ../../../images/Discover/introduction-nrsdk01.jpg
.. |image2| image:: ../../../images/Discover/introduction-nrsdk01.jpg


